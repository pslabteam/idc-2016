%==============================================================================
\section{The \spat{} Coordination Model and Language}
\labelsec{model}
%==============================================================================

In this section we  introduce the main elements of the \spat{} model and its coordination language by showing how it simply enhances the basic \linda{} model (\xss{basics}).
%
We discuss its extension towards mobility (\xss{mobility}), then demonstrate how the model effectively tackles diverse sorts of spatial coordination issues through some simple examples (\xss{examples}).

For the sake of simplicity, in the remainder of this section we adopt first-order logic notation for the specification of both tuples and tuple templates -- as in logic-based coordination models like Shared Prolog \cite{sharedprolog-toplas13} and \tucson{} / \respect{} \cite{respect-scico2001,tucson-aamas99} -- where logic variables allow for partial specification of templates, and unification works as the matching mechanism.
%
Accordingly, we adopt ground terms for regions (and locations), general logic term for region templates, and unification as their matching mechanism.

Since our focus here are the coordination issues, we do not explore the language issues involved in the description of location and regions---which are indeed essential for the application of \spat{} to real-world scenarios, but at the same time orthogonal with respect to coordination issues.
%
As a result, locations and regions are simply represented by means of identifiers (logic terms), whereas their matching is based on their associated spatial properties.

%-------------------------------------------------------------------------------
\subsection{Basics}
\labelssec{basics}
%-------------------------------------------------------------------------------

\begin{figure}[!t]
	\centering
	\includegraphics[width=\textwidth]{img/idea}
	\caption{Spatial tuples as information layer augmenting the physical reality.}
	\labelfig{idea}
\end{figure}

%---------------------------------------
\subsubsection{Spatial tuples.}
\labelsssec{tuples}
%---------------------------------------
\spat{} deals first of all with spatial tuples.
%
A \emph{spatial tuple} is a tuple associated to a spatial information. 
%
Spatial information can be of any sort, such as: GPS positioning (latitude, longitude, altitude), administrative (via Zamboni 33, Bologna, Italy), organisational (Room 5 of the DISI building at the University of Bologna), etc.---whatever the case, it anyway associates the tuple to some \emph{location} or \emph{region} in the \emph{physical space}.


As a result, a spatial tuple in principle \emph{decorates} physical space, and can work as the basic mechanism for \emph{augmenting reality} with information of any sort (see \xf{idea}).
%
Once a spatial tuple is associated to a region or location, its information can be thought of as describing properties of any sort that can be attributed to that specific portion of the physical space---thus implicitly adding observable properties to the physical space itself.
%
By accessing the tuple with \spat{} tuple-based mechanisms, the information can be observed by any sort of agent dealing with the specific physical space, so as to possibly behave accordingly.

%
% [ALE] AFTER REVIEWS ---
%
In tuple space based models, the \emph{communication language} is used to define the syntax of information content in tuples, as well as the matching mechanism between tuples and tuple templates.
%
Besides, in \spat{} a \emph{space-description language} is introduced to specify the spatial information decorating the tuples and the corresponding matching mechanism.
%
This spatial language is orthogonal to the communication language, and is meant to provide the basic ontology defining spatial concepts---such as locations, regions, places. 
%
To keep the description simple and general, in the remainder of the paper we will adopt an abstract spatial language, based on the  intuitive concepts of point-wise \emph{location} (no extension) and spatial \emph{region} (with an extension).
%
% ----

%---------------------------------------
\subsubsection{Spatial primitives.}
\labelsssec{primitives}
%---------------------------------------
As in any tuple-based model, \spat{} basic operations are \code{out(t)}, \code{rd(tt)}, \code{in(tt)}, where \code{t} is a tuple, \code{tt} a tuple template---see \xf{model} for a first intuition of how they are supposed to work.

\begin{figure}[t]\centering
	\includegraphics[width=\textwidth]{img/model}
	\caption{Agents inserting and retrieving spatial tuples. \emph{(Left)} An \textsf{out} placing the spatial tuple in a specific location; an \textsf{in} retrieving a tuple specifying a region.  \emph{(Right)} An \textsf{out} placing the spatial tuple in a region; a \textsf{rd} retrieving a tuple from the current location.}
	\labelfig{model}
\end{figure}

%
% OUT
%
Spatial tuples are emitted -- that is, tuples are associated to a region or location \code{r} -- by means of an \code{out} operation.
%
The following invocation
%
\begin{center}\code{out(t @ r)}\end{center}
%
emits spatial tuple \code{t @ r}---that is, tuple \code{t} associated to region (or location) \code{r}.

For instance, if \code{p1} is a location and \code{r2} a region, simple examples of \code{out} are
%
\begin{center}\code{out(info(message) @ p1)}~~~~~~\code{out(floor(wet) @ r2)}\end{center}
%
respectively associating tuple \code{info(message)} to the physical position of location \code{p1}, and decorating region \code{r2} with tuple \code{floor(wet)}.


%
% IN, RD, INP, RDP
%
In tuple-based models, \emph{getter} operations -- \code{in(tt)}, \code{rd(tt)} -- look for tuples matching with \emph{tuple template} \code{tt}.
%
In \spat{} they are extended with the ability to associatively explore the physical space, through the notion of \emph{region template}: that is, the ability to describe either the region or the location in space also in a partial way, and to find tuples in the matching regions / locations.


Accordingly, the basic forms of getter operation in \spat{} are
%
\begin{center}\code{rd(tt @ rt)}~~~~~~~~\code{in(tt @ rt)}\end{center}
%
Both operations look for a tuple \code{t} matching tuple template \code{tt} in any location or region of space \code{r} matching spatial template \code{rt}: as in standard \linda{}, \code{in} consumes the spatial tuple matching the template, whereas \code{rd} just returns it.
%
Following the standard semantic of tuple-based models, getter primitives in \spat{} are \emph{(i)} \emph{suspensive} -- if no tuple matching \code{tt} is found in any region or location matching \code{rt}, then the operation is blocked until a matching tuple is made available somehow -- and \emph{(ii)} \emph{non-deterministic}---if more than one tuple matching the tuple template is found in a region or location matching the spatial template, then one of them is returned non-deterministically.
%
For instance, 
%
\begin{center}\code{rd(floor(F) @ p1)}\end{center}
%
looks for a tuple of the form \code{floor(F)} associated to location \code{p1}---if any tuple matching is found there, it is returned, otherwise the operation is suspended.

If no tuple of the form \code{floor/1} occur in region \code{r2}, then \code{rd(floor(F) @ r2)} would block, to be subsequently resumed when an \code{out(floor(dirty) @ p1)} is performed, since $\code{p1} \in \code{r2}$ -- so, location \code{p1} spatially matches region (template) \code{r2} -- and tuple \code{floor(dirty)} matches tuple template \code{floor(F)} with substitution \code{\{F/dirty\}}---spatial tuple \code{floor(dirty) @ p1} would be returned, in case.

%
% PREDICATIVE and BULK
%
The \emph{predicative} and \emph{bulk} extensions -- represented by the primitives \code{inp(tt @ rt)}, \code{rdp(tt @ rt)} and \code{rd\_all(tt @ rt, L)}, \code{in\_all(tt @ rt, L)} -- are defined analogously, and as such they do not require further discussion in this context.



%-------------------------------------------------------------------------------
\subsection{Mobility}
\labelssec{mobility}
%-------------------------------------------------------------------------------
Spatial tuples can be associated to locations and regions either directly or indirectly.
%
The above-defined notions and operations directly associated a spatial tuple \code{t} to a region / location \code{r}; in \spat{}, however, a tuple can be associated to a system entity with either a position or an extension in the physical space.
%
In the following, we will often refer to these entities as \emph{situated components}.

So, if \code{id} identifies a component in a \spat{} system (a software artefact, a physical device), and \code{id} is a situated component, a spatial tuple \code{t} can be associated to \code{id} by means of an \code{out} operation of the form
%
\begin{center}\code{out(t @ id)}\end{center}
%
associating \code{t} to whichever is the region / location \code{r} where \code{id} is placed.

One of the main point here is that such an indirect association holds also when the region where \code{id} is located changes over time---that is, when \code{id} refers to a \emph{mobile} entity: until it is removed by an \code{in} operation of some sort, \code{t} will be associated to whatever place in space \code{id} is situated during its life cycle.

By assuming, for the sake of simplicity, that logic terms are used also for identifiers, if \code{car(1)}, \code{car(2)}, and \code{car(admin)} are situated components in a \spat{} system, operation
%
\begin{center}\code{out(auth(level(max)) @ car(admin))}\end{center}
%
decorates component \code{car(admin)} with tuple \code{auth(level(max))}, which is associated indirectly to \code{car(admin)} position from now on, thus following any possible motion of \code{car(admin)}.

As one may easily expect, also getter primitives are extended accordingly, allowing identifiers of situated components to be used as their targets.
%
Then
%
\begin{center}\code{rd(authorisation(level(L)) @ car(C))}~~~~~~\code{in(authorisation(level(\_)) @ car(admin))}\end{center}
%
respectively \emph{(left)} reads the authorisation level \code{L} of any situated component \code{car(C)} (non-deterministically), and \emph{(right)} removes any authorisation from \code{car(admin)}, wherever it is located in space.

Also, an implicit form for getter primitives is available to agents actually \emph{associated to situated components}: e.g., \spat{} agents upon a mobile device.
%
So, if agent \code{a} executes in component \code{id}, and \code{id} is in location \code{p}, operations of the form
%
\begin{center}\code{out(t @ here)}~~~~~~~~\code{out(t @ me)}\end{center}
%
associate tuple \code{t} to current \code{id} location \code{p}.
%
In the former case (\code{here}), the spatial tuple is permanently associated to \code{p}, even though \code{id} would subsequently move. 
%
In the latter case  (\code{me}), the spatial tuple would be associated to \code{p} just for the time \code{id} stays in \code{p}, then following any motion of \code{id}. 

In the same way, operations of the form
%
\begin{center}\code{rd(tt @ here)}~~~~~~~~\code{rd(tt @ me)}\\\code{in(tt @ here)}~~~~~~~~\code{in(tt @ me)}\end{center}
%
just return one tuple matching template \code{tt} if and when available at the current location of the situated component hosting the executing agent.

More articulated notations to specify / retrieve spatial tuples, situated components identifiers, and their positions in space altogether are available---but are not extensively discussed in the following, being not essential here.


%-------------------------------------------------------------------------------
\subsection{Space-based Coordination in \spat{}: Simple Examples}
\labelssec{examples}
% ------------------------------------------------------------------------------
Few simple examples may help understanding how the basic \spat{} model affects systems, effectively implementing different patterns of \emph{spatial coordination}.

%---------------------------------------
\subsubsection{Breadcrumbs.}
\labelsssec{breadcrumbs}
%---------------------------------------
A \spat{} agent over a mobile device may simply implement the breadcrumbs pattern.
%
For instance, agent \code{hansel} could hold a counter \code{C}, repeatedly increment it after a given period of time, and deposit a spatial tuple \code{wasHere/2} just after, while moving, with an operation
%
\begin{center}\code{out(wasHere(hansel, C) @ here)}\end{center}
%
As a result, the trajectory of agent \code{hansel} could be observed by observing the spatial distribution of tuples \code{wasHere/2}, and possibly traced back.


%---------------------------------------
\subsubsection{Awareness.}
\labelsssec{awareness}
%---------------------------------------
\spat{} enables basic forms of spatial awareness.
%
For instance, if a number of mobile devices is set to meet in region \code{meeting}, a \code{meetingControl} agent could check the arrival of all the expected devices by assuming that each of them would deposit a tuple through \code{out(hereIAm(id) @ here)}, and getting all of them by repeatedly performing an 
%
\begin{center}\code{in(hereIAm(Device) @ meeting)}\end{center}
%
until all the devices expected actually show up. 
%
It should be noted that while devices have to be situated components -- so they can use the implicit form of the \spat{} primitive --, \code{meetingControl} can be an agent of any sort, since the explicit form of the \spat{} primitive does not require situation.

%---------------------------------------
\subsubsection{Situated knowledge sharing.}
\labelsssec{situatedknowledge}
%---------------------------------------
\spat{} makes it simple to design time-uncoupled communication that requires situated knowledge sharing.
%
As a basic mechanism, in fact, \spat{} allows tuples to be situated (in a location, in a region of space) with a single operation invocation.
%
So, an agent is allowed to send a message to all agents located in some region indirectly, by representing the message as a spatial tuple associated to such a region. 

For instance, if agent \code{control\_room} in a rescue operation needs to warn all the rescuers about the presence of some injured people in some \code{region}, it could perform the invocation
%
\begin{center}\code{out(warning(injured) @ region)}\end{center}
%
so that all rescuer agents situated in \code{region}, waiting for local warnings upon a
%
\begin{center}\code{rd(warning(W) @ here)}\end{center}
%
would immediately receive the news.

%---------------------------------------
\subsubsection{Spatial synchronisation and mutual exclusion.}
\labelsssec{synchro}
%---------------------------------------
For \emph{spatial synchronisation} we mean synchronising actions of situated agents based on their position: e.g., an agent A would start some task as soon as an agent B arrives in some place.

Using again the meeting example above, an agent may decide to leave for a meeting only when any other agent reaches the meeting point through an invocation of the form:
%
\begin{center}\code{rd(hereIAm(\_) @ meeting)}\end{center}
%
%
and waiting for the proper spatial tuple to be returned.

%
% SPATIAL MUTEX
%
As a meaningful case, \emph{spatial mutual exclusion} -- for ruling the access of agents to some physical region -- can be achieved quite simply in \spat{}.
%
E.g., in order to allow just one agent at a time in a region (\code{mutex\_region}), to enter \code{mutex\_region} an agent could be required to get a tuple \code{lock} situated there, to be released as soon as the agent exits the region, respectively through
%
\begin{center}\code{in(lock @ mutex\_region)}~~~~~~~~~~\code{out(lock @ mutex\_region)}\end{center}

\begin{figure}[t]\centering
	\includegraphics[width=0.40\textwidth]{img/philo2}
	\caption{Seats and table as regions for Spatial Dining Philosophers}
	\labelfig{dining}
\end{figure}

%---------------------------------------
\subsubsection{Spatial Dining Philosophers.}
\labelsssec{dining}
%---------------------------------------
A slight variation of the Dining Philosopher example can be easily adapted to showcase spatial coordination in \spat{}.
%
We do not focus here on deadlock issues, by simply assuming a trivial ticket-based solution where $N-1$ \code{ticket @ table} spatial tuples are initially in place, and each of the $N$ philosophers enters the system with a \code{in(ticket @ table)}.

As shown in \xf{dining}, the chopsticks of a 4-Dining Philosophers problem are represented as spatial tuples \code{chop} situated in positions \code{p1}, \code{p2}, \code{p3}, and \code{p4}---\code{chop@p1}, \code{chop@p2}, \code{chop@p3}, \code{chop@p4}.
%
All of them are placed on the \code{table} region ($\code{p1},\code{p2},\code{p3},\code{p4} \in \code{table}$), and each of them is shared by two adjacent \code{seat}s---so, for instance, $\code{p1} \in \code{seat4}\cap\code{seat1}$, $\code{p2} \in \code{seat1}\cap\code{seat2}$, and so on.

Thus, for a philosopher to eat from \code{seat1}, it is enough to move there, invoke \code{in(ticket @ table)}, then perform twice the operation 
%
\begin{center}\code{in(chop @ seat1)}\end{center}
%
As a result, the philosopher would receive the two spatial tuples \code{chop@p1} and \code{chop@p2} -- in any order, due to non-determinism -- given that positions $\code{p1}$ and $\code{p2}$ spatially match with region template $\code{seat1}$.

Once both \code{chop@p1} and \code{chop@p2} have been obtained, the eating phase can start.
%
As one may expect, releasing both tuples with 
%
\begin{center}\code{out(chop @ p1)}, \code{out(chop @ p2)}\end{center}
%
and the ticket with an \code{out(ticket @ table)} ends the eating phase properly.



